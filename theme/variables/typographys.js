export default {
  FONT_FAMILIES: {
    PRIMARY: '"supermarket"',
    // SECONDARY: '"Roboto"',
  },

  FONT_SIZE_PX: {
    MN: '10px',
    TN: '12px',
    XXS: '13px',
    XS: '14px',
    SM: '16px',
    MD: '17px',
    LG: '20px',
    XL: '24px',
    XXL: '26px',
    BG: '30px',
    LBG: '35px',
    SHG: '40px',
    HG: '48px',
    MS: '64px',
  },

  FONT_WEIGHT: {
    SUPERMARKET_NORMAL: 'normal',
    SUPERMARKET_BOLD: 'bold',
    SUPERMARKET_SEMI_BOLD: 600,
    SUPERMARKET_LIGHT: 300,
    SUPERMARKET_THIN: 100,
    SUPERMARKET_MEDIAN: 500,
  },
};
