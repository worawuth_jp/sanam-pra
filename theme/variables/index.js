import COLOR from './color';
import ELEMENTSIZE from './elementSize';
import TRANSITIONS from './transitions';
import TYPOGRAPHYS from './typographys';

export default { COLOR, ELEMENTSIZE, TRANSITIONS, TYPOGRAPHYS };
