import { createGlobalStyle } from 'styled-components';
import 'bootstrap/dist/css/bootstrap.min.css';
import variables from 'theme/variables';

export const Globalstyle = createGlobalStyle`
html,
  body {
    padding: 0;
    margin: 0;
    font-family: ${variables.TYPOGRAPHYS.FONT_FAMILIES.PRIMARY}, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
    font-size: 17px;
    font-weight: normal;
  }


  .my-button-primary{
    background-color: ${variables.COLOR.PRIMARY};
    color: ${variables.COLOR.PRIMARY_TEXT_COLOR};
  }

  .my-button-secorndary{

    background-color: ${variables.COLOR.SECORNDARY};
    color: ${variables.COLOR.SECORNDARY_TEXT_COLOR};
  }

  &.dropdown-theme{
    height: 36px !important;
    background: ${variables.COLOR.PRIMARY_TEXT_COLOR};
    border-radius: 10px !important;
    border-style: solid;
    border-width: thin;

    &:focus {
      outline: none !important;

      &:after {
        outline: none !important;
      }
    }

    &:active {
      outline: none !important;
    }
    &:hover {
      border-radius: 10px;
      height: 36px !important;
      background: ${variables.COLOR.NavThemeColor};
    }
  }

  .error-msg{
    display: block;
    color: red;
    margin-top: 0px;
  }

  .check-me label {
    font-size: 10px !important;
  }

  input:focus{
    outline: 0 none;
  }
`;
