import { Layout, MyInput, Button } from 'components';
import React from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import LoginStyle from './styled';

export default function Login() {
  return (
    <LoginStyle>
      <Head>
        <title>sanam-pra</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">OTP</h2>
          </Row>
          <Form className="col-md-4 ml-auto mr-auto mt-3">
            <Form.Group controlId="formUsername">
              <Col md="12" className="p-0 text-center otp-text">
                กรุณาใส่รหัสยืนยัน
                <Form.Label>(รหัสยืนยันจะถูกส่งไปทาง SMS เบอร์ 099xxxxxxx)</Form.Label>
              </Col>

              <Row className="col-md-12 ml-auto mr-auto">
                <MyInput type="text" className="p-2 mt-0 ml-auto mb-0 otp" />
                <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                <MyInput type="text" className="p-2 ml-2 mt-0 mr-auto mb-0 otp" />
              </Row>
              {/* <Form.Control type="email" placeholder="Enter email" /> */}
              <Form.Text className="error-msg ml-1 pt-0">phone number is required</Form.Text>
            </Form.Group>
            <Button isCenter className="my-button-primary none-border mb-3 col-md-12 mt-4" type="submit">
              ต่อไป
            </Button>
          </Form>
        </Container>
      </Layout>
    </LoginStyle>
  );
}
