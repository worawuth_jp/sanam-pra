import { Layout, MyInput, Button } from 'components';
import React from 'react';
import { Container, Form, Row } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import LoginStyle from './styled';

export default function Login() {
  return (
    <LoginStyle>
      <Head>
        <title>sanam-pra</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">เบอร์โทรศัพท์</h2>
          </Row>
          <Form className="col-md-4 ml-auto mr-auto mt-4">
            <Form.Group controlId="formUsername">
              <MyInput type="text" placeholder="phone number" className="p-2 col-md-12 mb-0" />
              {/* <Form.Control type="email" placeholder="Enter email" /> */}
              <Form.Text className="error-msg ml-1 pt-0">phone number is required</Form.Text>
            </Form.Group>
            <Button isCenter className="my-button-primary none-border col-md-12 mt-4" type="submit">
              ต่อไป
            </Button>
          </Form>
        </Container>
      </Layout>
    </LoginStyle>
  );
}
