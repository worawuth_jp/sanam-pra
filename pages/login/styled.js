import styled from 'styled-components';
import variables from 'theme/variables';

const LoginStyle = styled.div`
  font-size: ${variables.TYPOGRAPHYS.FONT_SIZE_PX.LG};

  .otp-text {
    color: ${variables.COLOR.GRAY_2};
    label {
      font-size: 17px;
      color: ${variables.COLOR.GRAY_1};
    }
  }

  .otp {
    width: 35px;
    text-align: center;
    font-size: 20px;
    color: ${variables.COLOR.PRIMARY};
  }

  .remember-me {
    display: flex !important;

    label {
      font-size: 18px;
      color: ${variables.COLOR.GRAY_2};
    }

    .reset-password {
      font-size: 18px;
      margin-left: auto;
      margin-right: 15px;
    }
  }
`;

export default LoginStyle;
