import { Layout, MyInput, Button } from 'components';
import React from 'react';
import { Container, Form, Row } from 'react-bootstrap';
import Head from 'next/head';
import Link from 'next/link';
import LoginStyle from './styled';

export default function Login() {
  return (
    <LoginStyle>
      <Head>
        <title>sanam-pra</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">Login</h2>
          </Row>
          <Form className="col-md-4 ml-auto mr-auto">
            <Form.Group controlId="formUsername">
              <MyInput type="text" placeholder="username" className="p-2 col-md-12 mb-0" />
              {/* <Form.Control type="email" placeholder="Enter email" /> */}
              <Form.Text className="error-msg ml-1 pt-0">username is required</Form.Text>
            </Form.Group>

            <Form.Group className="mt-2" controlId="formPassword">
              <MyInput type="password" placeholder="password" className="p-2 col-md-12" />
              <Form.Text className="error-msg ml-1 pt-0">password is required</Form.Text>
            </Form.Group>
            <Form.Group className="remember-me">
              <Form.Check className="checkme col-form-label" type="checkbox" />
              <label className="col-form-label">Remeber Me</label>
              <Link href="#">
                <a className="col-form-label reset-password">ลืมรหัสผ่าน</a>
              </Link>
            </Form.Group>
            <Button isCenter className="my-button-primary none-border col-md-12" type="submit">
              Login
            </Button>
          </Form>
        </Container>
      </Layout>
    </LoginStyle>
  );
}
