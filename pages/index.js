import { Layout, Product } from 'components';
import dayjs from 'dayjs';
import Head from 'next/head';
import React from 'react';
import { Container } from 'react-bootstrap';
import IndexStyle from './index/styled';

export default function Home() {
  const time = new Date('2021-08-21 12:20');
  const products = [
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
    {
      name: 'บอล บ้านโป่ง',
      icon: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      pro_img: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      upload_time: time,
      product_name: 'หลวงพ่อ',
      category_name: 'หมวดพระเครื่อง',
      prices: [99.0, 89.0],
      views: 289,
    },
  ];
  return (
    <IndexStyle>
      <Head>
        <title>sanam-pra</title>
      </Head>
      <Layout>
        <Container>
          <Product itemPerRow="4" products={products} />
        </Container>
      </Layout>
    </IndexStyle>
  );
}
