import { Layout } from 'components';
import Head from 'next/head';
import React from 'react';
import { Container, Row } from 'react-bootstrap';
import PrivacyStyle from './styled';

export default function Privacy() {
  return (
    <PrivacyStyle>
      <Head>
        <title>privacy</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">ข้อตกลงการสมัครสมาชิก</h2>
          </Row>
        </Container>
      </Layout>
    </PrivacyStyle>
  );
}
