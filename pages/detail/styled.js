import styled from 'styled-components';
import variables from 'theme/variables';

const DetailStyle = styled.div`
  .img-preview {
    height: 600px;
    width: auto;
  }

  .col-carousel {
    height: 100% !important;
  }

  .my-carousel {
    padding-left: 50px;
    padding-right: 50px;
    padding-bottom: 50px;
    padding-top: 50px;
    max-height: 700px;
    width: 100%;
    text-align: center;
    background: ${variables.COLOR.GRAY_5};
  }

  .carousel-icon {
    font-size: 50px;
    height: 100%;
  }

  .img-card {
    min-width: 100px;
    min-height: 100px;
    max-width: 100px;
    max-height: 100px;
    background: ${variables.COLOR.WHITECOLOR};
  }

  .upload-time {
    padding-top: 2px;
    text-align: right;
    color: ${variables.COLOR.GRAY_9};
  }

  .next-icon {
    position: absolute !important;
    height: 100%;
    width: 50%;
    right: 0px;
    background: ${variables.COLOR.WHITECOLOR};
    color: ${variables.COLOR.BLACKCOLOR};
    opacity: 50%;
  }

  .prev-icon {
    position: absolute !important;
    height: 100%;
    width: 50%;
    left: 0px;
    background: ${variables.COLOR.WHITECOLOR};
    color: ${variables.COLOR.BLACKCOLOR};
    opacity: 50%;
  }

  .white-content {
    background: ${variables.COLOR.WHITECOLOR};
    min-height: 700px;
    max-height: 700px;
    overflow-y: auto;
  }

  .detail {
    h5 {
      text-decoration: underline;
      color: black;
    }
    white-space: normal;
    word-break: break-all;
    min-height: 100px;
    max-height: 120px;
    color: ${variables.COLOR.BLACKCOLOR};
  }

  .bargain {
    h5 {
      text-decoration: underline;
      color: black;
      text-align: left;
    }
    text-align: center;

    .price {
      display: inline-block;
      text-align: center;
      font-weight: bold;
      font-size: 18px;
      min-width: 100px;
      padding-right: 10px;
      padding-left: 10px;
      background: ${variables.COLOR.GREEN_1};
    }

    .price-input {
      background: ${variables.COLOR.YELLOW_1};
      text-align: right;
      padding-right: 7px;
    }
  }

  .card-quatify {
    display: flexbox;
    width: 100%;
    align-items: center;
    min-height: 100px;
    max-height: 100px;
    background: ${variables.COLOR.BLUE_2};
    .flex-item {
      display: block;
      width: 100%;
      text-align: center;
      color: ${variables.COLOR.WHITECOLOR};
    }
  }

  .card-profile {
    display: flexbox;
    width: 100%;
    align-items: center;
    min-height: 100px;
    max-height: 400px;
    background: ${variables.COLOR.NavThemeColor};
    .flex-item {
      display: block;
      width: 100%;
      color: ${variables.COLOR.WHITECOLOR};
      img {
        height: 80px;
      }

      .profile {
        display: flex;
        flex-wrap: wrap;
      }

      .profile-item {
        display: block;
        flex-wrap: wrap;
      }

      .profile-name {
        font-size: 18px;
        display: inline;
        margin-bottom: 0px;
      }

      button {
        font-size: 15px !important;
      }

      .credit {
        color: ${variables.COLOR.WHITECOLOR};
        span {
          color: ${variables.COLOR.GREEN_1};
        }
      }

      .subscribe {
        color: ${variables.COLOR.GRAY_3};
        span {
          color: ${variables.COLOR.ORANGE_2};
        }
      }

      .status {
        font-weight: bold;
        &.online {
          color: ${variables.COLOR.GREEN_3};
        }
        &.offline {
          color: ${variables.COLOR.YELLOW_3};
        }
        padding-top: 0px;
        font-size: 14px;
      }
    }
  }

  .member-card {
    color: ${variables.COLOR.WHITECOLOR};
    min-height: 200px;
    width: 100%;
    background: ${variables.COLOR.BLACKCOLOR};

    .member-card-detail {
      display: flex;
      flex-wrap: wrap;
      .member-img {
        width: 125px;
        height: 125px;
      }
    }

    .member-content {
      //min-height: 125px;
      //max-height: 125px;
      margin-left: 10px;
      display: block;
      flex-wrap: wrap;

      span {
        color: ${variables.COLOR.SECORNDARY_TEXT_COLOR};
      }
    }
  }

  .bargain-detail {
    color: ${variables.COLOR.BLACKCOLOR};
    max-height: 170px;
    max-height: 250px;
    overflow-y: auto;
    padding-top: 10px;

    .list {
      font-size: 18px;
    }

    div {
      margin-top: 2px;
    }

    span {
      font-weight: bold;
      color: ${variables.COLOR.BLUE_1};

      &.time {
        color: ${variables.COLOR.ORANGE_1};
      }
    }
  }
`;

export default DetailStyle;
