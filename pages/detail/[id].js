import { Button, CommentBox, CommentItem, Layout, MyInput } from 'components';
import _ from 'lodash';
import Head from 'next/head';
import React, { useState } from 'react';
import { Carousel, Col, Container, Image, Row } from 'react-bootstrap';
import NavigateNextIcon from '@material-ui/icons/NavigateNext';
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import DetailStyle from './styled';
import classNames from 'classnames';

export default function Detail() {
  const product = {
    imgs: [
      {
        src: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAA1BMVEX///+nxBvIAAAASElEQVR4nO3BgQAAAADDoPlTX+AIVQEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADwDcaiAAFXD1ujAAAAAElFTkSuQmCC',
      },
      {
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      },
      {
        src: 'https://images.ctfassets.net/hrltx12pl8hq/7JnR6tVVwDyUM8Cbci3GtJ/bf74366cff2ba271471725d0b0ef418c/shutterstock_376532611-og.jpg',
      },
      {
        src: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png',
      },
    ],
  };

  const [index, setIndex] = useState(0);

  const nextIcon = (
    <span aria-hidden="true" className="next-icon">
      <NavigateNextIcon className="carousel-icon mt-auto mr-auto" />
    </span>
  );
  const prevIcon = (
    <span aria-hidden="true" className="prev-icon">
      <NavigateBeforeIcon className="carousel-icon mt-auto mr-auto" />
    </span>
  );

  const handleSelect = (selectedIndex, e) => {
    console.log(selectedIndex);
    setIndex(selectedIndex);
  };
  return (
    <DetailStyle>
      <Head>
        <title>product detail</title>
      </Head>
      <Layout>
        <Row>
          <Col md="7" className="col-carousel pb-0 mb-0">
            <Row>
              <Carousel className="my-carousel" activeIndex={index} onSelect={handleSelect} prevIcon={prevIcon} nextIcon={nextIcon}>
                {_.map(product.imgs, (item, index) => {
                  return (
                    <Carousel.Item className="bottom" key={`carousel_img_${index}`}>
                      <Image className="img-preview" src={item.src} rounded fluid />
                    </Carousel.Item>
                  );
                })}
              </Carousel>
            </Row>
          </Col>
          <Col md="5" className="white-content pb-0 mb-0">
            <div className="upload-time">
              อัพโหลด 28/08/2021
              <span className="ml-2">
                เวลา <span>10.21</span> น.
              </span>
            </div>
            <div className="detail mt-3">
              <h5>รายละเอียด</h5>
              <p>
                xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxsssssssssssssssssssssscccccccccccccccccsssssssssssxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
              </p>
            </div>

            <div className="bargain mt-3">
              <h5>ต่อรองราคา</h5>
              ราคาเปิด
              <div className="price ml-2 mr-2">200</div>
              บาท
            </div>

            <div className="bargain mt-2">
              ใส่ราคา
              <MyInput className="ml-2 mr-2 col-md-3 price-input right" />
              บาท
              <Button className="pl-2 pr-2 ml-2 price-btn">เสนอราคา</Button>
            </div>

            <Col className="bargain-detail border mt-3">
              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณมาย <span className="ml-2 mr-2">500</span> บาท เวลา <span className="ml-2 mr-2 time">10.30</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณเจ <span className="ml-2 mr-2">400</span> บาท เวลา <span className="ml-2 mr-2 time">10.00</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณอั้ม <span className="ml-2 mr-2">300</span> บาท เวลา <span className="ml-2 mr-2 time">09.30</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณมาย <span className="ml-2 mr-2">500</span> บาท เวลา <span className="ml-2 mr-2 time">10.30</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณเจ <span className="ml-2 mr-2">400</span> บาท เวลา <span className="ml-2 mr-2 time">10.00</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณอั้ม <span className="ml-2 mr-2">300</span> บาท เวลา <span className="ml-2 mr-2 time">09.30</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณมาย <span className="ml-2 mr-2">500</span> บาท เวลา <span className="ml-2 mr-2 time">10.30</span> น.
              </div>

              <div>
                <Button className="pl-2 pr-2 pt-0 pb-0 mr-2 bargain-btn">ปิด</Button>
                <FiberManualRecordIcon className="list" />
                คุณเจ <span className="ml-2 mr-2">400</span> บาท เวลา <span className="ml-2 mr-2 time">10.00</span> น.
              </div>
            </Col>
          </Col>
        </Row>
        <Row>
          <Col md="7">
            <Row>
              <div className="card-quatify">
                <div className="flex-item">ประกันพระแท้ตามหลักสากลภายในระยะเวลา 30 วัน</div>
              </div>
              <div className="card-profile">
                <div className="flex-item">
                  <Row className="col-md-12 profile">
                    <div className="ml-2 mr-2 profile-item">
                      <Image
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png"
                        roundedCircle
                        fluid
                      />
                    </div>
                    <div className="profile-item">
                      <div className="profile-name">บอล บ้านโป่ง</div>
                      <div className={classNames('status', { offline: true })}>ออนไลน์</div>
                      <Button className="my-button-secorndary pl-3 pr-3">แชทเลย</Button>
                      <Button className="my-button-secorndary pl-3 pr-3 ml-2">ดูข้อมูลส่วนตัว</Button>
                    </div>

                    <div className="profile-item ml-auto">
                      <div className="subscribe">
                        เข้าร่วมเมื่อ <span className="ml-2 mr-2">21/07/2021</span>
                      </div>
                      <div className="subscribe">
                        ผู้ติดตาม <span className="ml-2 mr-2">15</span> คน
                      </div>
                      <div className="credit">
                        เครดิต <span>+200</span>
                      </div>
                    </div>
                  </Row>
                </div>
              </div>
            </Row>
          </Col>
          <Col md="5">
            <Row>
              <div className="member-card p-2">
                <div>บัตรประจำตัวสมาชิก(Premium)</div>
                <div className="member-card-detail">
                  <Image
                    className="member-img mt-2"
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Solid_grey.svg/1200px-Solid_grey.svg.png"
                    rounded
                    fluid
                  />
                  <div className="member-content">
                    <div>
                      รหัสสมาชิก : <span>xxxxxxxxxx</span>
                    </div>
                    <div>
                      ชื่อ : <span>xxxxxxxxxx</span>
                    </div>
                    <div>
                      ที่อยู่ : <span>xxxxxxxxxx</span>
                    </div>
                    <div>
                      เลขบัญชี : <span>xxxxxxxxxx</span>
                    </div>
                  </div>
                </div>
              </div>
            </Row>
          </Col>
        </Row>

        <Row className="mt-3">
          <CommentBox>
            <CommentItem name="นาย ก">555555</CommentItem>
            <CommentItem name="นาย ก">555555</CommentItem>
            <CommentItem name="นาย ก">555555</CommentItem>
          </CommentBox>
        </Row>
      </Layout>
    </DetailStyle>
  );
}
