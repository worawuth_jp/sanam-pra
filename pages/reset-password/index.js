import { Button, Layout, MyInput } from 'components';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';
import { Container, Form, Row } from 'react-bootstrap';
import ResetPasswordStyle from './styled';

export default function ResetPassword() {
  return (
    <ResetPasswordStyle>
      <Head>
        <title>reset password</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">ตั้งรหัสผ่านใหม่</h2>
          </Row>
          <Form className="col-md-4 ml-auto mr-auto mt-3">
            <Form.Group controlId="formPassword">
              <MyInput type="text" placeholder="new password" className="p-2 col-md-12 mb-0" />
              {/* <Form.Control type="email" placeholder="Enter email" /> */}
              <Form.Text className="error-msg ml-1 pt-0">new password is required</Form.Text>
            </Form.Group>

            <Form.Group className="mt-2" controlId="formNewPassword">
              <MyInput type="password" placeholder="confirm password" className="p-2 col-md-12" />
              <Form.Text className="error-msg ml-1 pt-0">confirm password is required</Form.Text>
            </Form.Group>

            <Button isCenter className="my-button-primary none-border mt-3 mb-4 col-md-12" type="submit">
              Reset Password
            </Button>
          </Form>
        </Container>
      </Layout>
    </ResetPasswordStyle>
  );
}
