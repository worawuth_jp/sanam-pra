import { Fragment } from 'react';
import Head from 'next/head';
import { Globalstyle } from 'styles/globals';
import { wrapper } from 'redux/store';

function MyApp({ Component, pageProps }) {
  return (
    <Fragment>
      <Globalstyle />
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, shrink-to-fit=no"></meta>

        {/* Font */}
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
        <link rel="stylesheet" type="text/css" href="/fonts.css"></link>
      </Head>
      <Component {...pageProps} />
    </Fragment>
  );
}

export default wrapper.withRedux(MyApp);
