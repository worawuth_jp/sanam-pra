import Head from 'next/head';
import React from 'react';
import { Button, Layout, MyInput } from 'components';
import SignUpStyle from './styled';
import { Col, Container, Form, Row } from 'react-bootstrap';
import DropZone from 'components/DropZone';

export default function page3() {
  return (
    <SignUpStyle>
      <Head>
        <title>verify</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">ยืนยันตัวตน</h2>
          </Row>
          <Form className="col-md-12 ml-auto mr-auto">
            <Row>
              <Col className="mr-auto ml-auto" md="10" sm="12" xs="12">
                <Row>
                  <Col md="6" className="ml-auto mr-auto">
                    <Form.Group controlId="formIDCard">
                      <Form.Label className="mb-0">สำเนาบัตรประชาชน</Form.Label>
                      {/* <MyInput type="text" placeholder="ID Card" className="p-2 mt-0 col-md-12 mb-0" /> */}
                      <DropZone className="p-2 mt-0 ml-2 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">require JPG/PNG Format</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col md="6" className="ml-auto mr-auto">
                    <Form.Group controlId="formIDCard">
                      <Form.Label className="mb-0">KYC</Form.Label>
                      {/* <MyInput type="text" placeholder="ID Card" className="p-2 mt-0 col-md-12 mb-0" /> */}
                      <DropZone className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">require JPG/PNG Format</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col md="6" className="ml-auto mr-auto">
                    <Button type="submit" isCenter className="my-button-primary none-border col-md-12 col-sm-10 col-xs-12 mt-2 mb-3">
                      ยืนยันและสมัครสมาชิก
                    </Button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>
        </Container>
      </Layout>
    </SignUpStyle>
  );
}
