import styled from 'styled-components';
import variables from 'theme/variables';

const SignUpStyle = styled.div`
  font-size: ${variables.TYPOGRAPHYS.FONT_SIZE_PX.LG};

  label {
    font-size: 18px;
    color: ${variables.COLOR.GRAY_2};
  }

  .otp {
    width: 35px;
    text-align: center;
    font-size: 20px;
    color: ${variables.COLOR.PRIMARY};
  }

  &.dropdown-theme {
    border-radius: 10px !important;

    border-width: thin !important;
    height: 36px !important;

    padding: 2px 2px 2px 10px !important;
  }
`;

export default SignUpStyle;
