import React from 'react';
import SignUpStyle from './styled';
import Head from 'next/head';
import { Button, Layout, MyInput } from 'components';
import { Col, Container, Form, Row } from 'react-bootstrap';

export default function SignUp() {
  return (
    <SignUpStyle>
      <Head>
        <title>signup</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">สร้างบัญชี</h2>
          </Row>
          <Form className="col-md-12 ml-auto mr-auto">
            <Row>
              <Col className="mr-auto" md="4" sm="12" xs="12">
                <Form.Group controlId="formUsername">
                  <Form.Label className="mb-0">Username</Form.Label>
                  <MyInput type="text" placeholder="username" className="p-2 mt-0 col-md-12 mb-0" />
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">username is required</Form.Text>
                </Form.Group>

                <Form.Group controlId="formPassword">
                  <Form.Label className="mb-0">รหัสผ่าน</Form.Label>
                  <MyInput type="password" placeholder="password" className="p-2 col-md-12" />
                  <Form.Text className="error-msg ml-1 pt-0">password is required</Form.Text>
                </Form.Group>

                <Form.Group controlId="formConfPassword">
                  <Form.Label className="mb-0">ยืนยันรหัสผ่าน</Form.Label>
                  <MyInput type="password" placeholder="confirm password" className="p-2 col-md-12" />
                  <Form.Text className="error-msg ml-1 pt-0">confirm password is required</Form.Text>
                </Form.Group>

                <Form.Group controlId="formFirstname">
                  <Form.Label className="mb-0">ชื่อจริง</Form.Label>
                  <MyInput type="text" placeholder="ชื่อจริง" className="p-2 mt-0 col-md-12 mb-0" />
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">firstname is required</Form.Text>
                </Form.Group>

                <Form.Group controlId="formLastname">
                  <Form.Label className="mb-0">นามสกุล</Form.Label>
                  <MyInput type="text" placeholder="นามสกุล" className="p-2 mt-0 col-md-12 mb-0" />
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">lastname is required</Form.Text>
                </Form.Group>
              </Col>

              <Col className="ml-auto" md="4" sm="12" xs="12">
                <Form.Group controlId="formShowName">
                  <Form.Label className="mb-0">ชื่อที่ใช้แสดงบนเว็บ</Form.Label>
                  <MyInput type="text" placeholder="ชื่อที่ใช้แสดงบนเว็บ" className="p-2 mt-0 col-md-12 mb-0" />
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">showname is required</Form.Text>
                </Form.Group>
                <Form.Group controlId="formPhoneNummer">
                  <Form.Label className="mb-0">เบอร์โทรศัพท์</Form.Label>
                  <MyInput type="text" placeholder="phone number" className="p-2 mt-0 col-md-12 mb-0" />
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">phonenumber is required</Form.Text>
                </Form.Group>

                <Button type="button" isCenter className="my-button-primary none-border col-md-6 col-sm-10 col-xs-12 mt-2 mb-3">
                  รับ SMS
                </Button>

                <Form.Group controlId="formPhoneNummer">
                  <Form.Label className="mb-0">OTP:SMS ยืนยันตัวตน</Form.Label>
                  <Row className="col-md-12">
                    <MyInput type="text" className="p-2 mt-0  mb-0 otp" />
                    <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                    <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                    <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                    <MyInput type="text" className="p-2 ml-2 mt-0  mb-0 otp" />
                    <MyInput type="text" className="p-2 ml-2 mt-0 mb-0 otp" />
                  </Row>
                  {/* <Form.Control type="email" placeholder="Enter email" /> */}
                  <Form.Text className="error-msg ml-1 pt-0">OTP is required</Form.Text>
                </Form.Group>

                <Button type="submit" isCenter className="my-button-primary none-border col-md-10 col-sm-10 col-xs-12 mt-2 mb-3">
                  ยืนยันและสมัครสมาชิก
                </Button>
              </Col>
            </Row>
          </Form>
        </Container>
      </Layout>
    </SignUpStyle>
  );
}
