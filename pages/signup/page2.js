import { Button, Layout, MyInput, SelectBox } from 'components';
import Head from 'next/head';
import React from 'react';
import { Col, Container, Form, Row } from 'react-bootstrap';
import SignUpStyle from './styled';

export default function page2() {
  const options = [
    { label: 'ไม่ระบุ', value: '0' },
    { label: 'ชาย', value: '1' },
    { label: 'หญิง', value: '2' },
  ];

  return (
    <SignUpStyle>
      <Head>
        <title>signup</title>
      </Head>
      <Layout WhiteContent>
        <Container>
          <Row>
            <h2 className="ml-auto mr-auto">ยืนยันตัวตนขั้นตอนที่ 2</h2>
          </Row>
          <Form className="col-md-12 ml-auto mr-auto">
            <Row>
              <Col className="mr-auto ml-auto" md="10" sm="12" xs="12">
                <Row>
                  <Col md="6">
                    <Form.Group controlId="formIDCard">
                      <Form.Label className="mb-0">หมายเลขบัตรประจำตัวประชาชน</Form.Label>
                      <MyInput type="text" placeholder="ID Card" className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">ID Card is required</Form.Text>
                    </Form.Group>
                  </Col>

                  <Col md="3">
                    <Form.Group controlId="formIDCard">
                      <Form.Label className="mb-0">วันเกิด</Form.Label>
                      <MyInput type="text" placeholder="BirthDate" className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">BirthDate is required</Form.Text>
                    </Form.Group>
                  </Col>

                  <Col md="3">
                    <Form.Group controlId="formIDCard">
                      <Form.Label className="mb-0 ">เพศ</Form.Label>
                      <SelectBox className="ml-0 mt-0" value={options[0]} options={options} />

                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">Gender is required</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col md="6">
                    <Form.Group controlId="formAddress">
                      <Form.Label className="mb-0">ที่อยู่</Form.Label>
                      <MyInput type="text" placeholder="ที่อยู่" className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">Address is required</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col md="6">
                    <Form.Group controlId="formProvince">
                      <Form.Label className="mb-0">จังหวัด</Form.Label>
                      <MyInput type="text" placeholder="จังหวัด" className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">Province is required</Form.Text>
                    </Form.Group>
                  </Col>

                  <Col md="6">
                    <Form.Group controlId="formDistrict">
                      <Form.Label className="mb-0">อำเภอ</Form.Label>
                      <MyInput type="text" placeholder="District" className="p-2 mt-0 col-md-12 mb-0" />
                      <Form.Text className="error-msg ml-1 pt-0">District is required</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>

                <Row>
                  <Col md="6">
                    <Form.Group controlId="formSubdistrict">
                      <Form.Label className="mb-0">ตำบล</Form.Label>
                      <MyInput type="text" placeholder="ตำบล" className="p-2 mt-0 col-md-12 mb-0" />
                      {/* <Form.Control type="email" placeholder="Enter email" /> */}
                      <Form.Text className="error-msg ml-1 pt-0">Subdistrict is required</Form.Text>
                    </Form.Group>
                  </Col>

                  <Col md="6">
                    <Form.Group controlId="formDistrict">
                      <Form.Label className="mb-0">รหัสไปรษณีย์</Form.Label>
                      <MyInput type="text" placeholder="Post Number" className="p-2 mt-0 col-md-12 mb-0" />
                      <Form.Text className="error-msg ml-1 pt-0">Post Number is required</Form.Text>
                    </Form.Group>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Form>

        </Container>

      </Layout>
    </SignUpStyle>
  );
}
