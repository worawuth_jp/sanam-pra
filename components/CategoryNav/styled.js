import styled, { css } from 'styled-components';
import variables from 'theme/variables';

export const CategoryStyle = styled.div`
  position: relative;
  display: flex;
  //padding: 15px 10px 10px 10px;
  //height: ${variables.ELEMENTSIZE.CATEGORY_NAV_HEIGHT};
  left: 10%;
  right: 10%;
  width: 80%;
  ${(props) => {
    return css`
      top: calc((100% - ${`${props.categoryHeight}px`}));
    `;
  }}
  //top: calc((100% - ${variables.ELEMENTSIZE.CATEGORY_NAV_HEIGHT}));

  span {
    width: 100%;
    margin-left: auto;
    color: ${variables.COLOR.SECORNDARY_TEXT_COLOR};
  }

  img {
    display: block;
    width: 65px;
    height: 65px;
  }

  .menu:first-child {
    margin-left: auto;
  }

  .menu:last-child {
    margin-right: auto;
  }

  .menu {
    text-align: center;
    padding: 10px 10px 10px 10px;
    margin-left: 10px;
    cursor: pointer;
  }

  .menu:hover {
    background: ${variables.COLOR.NavThemeColor};
  }

  @media only screen and (max-width: 768px) {
    height: ${variables.ELEMENTSIZE.CATEGORY_NAV_HEIGHT_XS};
    ${(props) => {
      return css`
        top: calc((100% - ${`${props.categoryHeight}px`}));
      `;
    }}

    img {
      display: block;
      width: 30px;
      height: 30px;
    }
  }

  @media only screen and (max-width: 1200px) and (min-width: 769px) {
    height: ${variables.ELEMENTSIZE.CATEGORY_NAV_HEIGHT_SM};
    ${(props) => {
      return css`
        top: calc((100% - ${`${props.categoryHeight}px`}));
      `;
    }}

    img {
      display: block;
      width: 45px;
      height: 45px;
    }
  }
`;
