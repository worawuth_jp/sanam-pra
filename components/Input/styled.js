import styled from 'styled-components';

export const InputStyle = styled.input`
  border-radius: 10px;
  border-color: '#D7D7D7';
  border-width: thin;
  height: 36px;
  font-size: 17px;
  color: '#8A8A8A';
  padding: 2px 2px 2px 10px;

  :focus {
    outline: none;
  }
`;
