import React from 'react';
import { InputStyle } from './styled';

export default function MyInput({ className, value, ...props }) {
  return <InputStyle value={value} className={className} {...props}></InputStyle>;
}
