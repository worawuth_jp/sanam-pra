import styled from 'styled-components';
import VARIABLES from '../../theme/variables';

// Wrapper
// ============================================================
export const DropZoneStyle = styled.div`
  /* Parent styles
  ------------------------------- */
  width: 100%;
  margin: 0 auto;
  cursor: pointer;

  /* Child element styles
  ------------------------------- */
  .dropzone-wrapper {
    width: 100%;
    min-height: 130px;
    background-color: ${VARIABLES.COLOR.GRAY_3};
    border: 1px dashed #c2c2c2;
    border-radius: 5px;
    position: relative;
    transition: all ${VARIABLES.TRANSITIONS.FAST} ease;

    &:hover {
      background-color: ${VARIABLES.COLOR.GRAY_6};
      /* p {
        color: ${VARIABLES.COLOR.WHITECOLOR};
      } */

      /* .dropzone {
        p {
          color: ${VARIABLES.COLOR.GRAY_5};
        }
      } */
    }
  }

  .dropzone {
    width: 100%;
    height: 100%;
    position: absolute;
    left: 0;
    right: 0;
    top: 0;
    bottom: 0;
    display: flex;
    align-items: center;
    justify-content: center;

    p {
      margin: 0;
      font-size: 20px;
      color: ${VARIABLES.COLOR.GRAY_5};
      text-align: center;
      transition: all ${VARIABLES.TRANSITIONS.FAST} ease;
    }
  }

  .dropzone-display-wrapper {
    margin-top: 30px;

    .file-list-wrapper {
      border: 1px solid ${VARIABLES.COLOR.GRAY_7};
      padding: 14px 30px;
      border-radius: 5px;
      display: flex;
      align-items: flex-start;
      margin-bottom: 10px;

      &:last-child {
        margin-bottom: 0;
      }
    }

    .file-detail {
      flex: 1 1 auto;
      //padding-top: 4px;
      //max-width: calc(100% - 109px);

      .file-name {
        max-width: 100%;
        overflow: hidden;
        text-overflow: ellipsis;
        //white-space: nowrap;
        word-break: break-all;
      }

      .file-size {
        color: ${VARIABLES.COLOR.GRAY_9};
        font-size: 12px;
      }
    }

    .file-type-icon {
      margin-left: 10px;
      flex: 0 0 50px;
      height: auto;
      margin-right: 20px;
    }

    .file-delete-icon {
      flex: 0 0 14px;
      margin-left: 15px;
      //filter: brightness(2) contrast(0);
    }
  }

  /* Modifiers
  ------------------------------- */

  /* State
  ------------------------------- */

  /* Media queries
  ------------------------------- */
  @media (max-width: 1440.98px) {
    min-width: 300px;
    max-width: 540px;

    .dropzone-wrapper {
      min-height: 110px;
    }

    .dropzone {
      p {
        font-size: 17px;
      }
    }
  }

  @media (max-width: 1280.98px) {
  }

  @media (max-width: 1024.98px) {
  }
`;
