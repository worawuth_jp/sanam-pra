import classNames from 'classnames';
import React, { useState } from 'react';
import { useDropzone } from 'react-dropzone';
import { DropZoneStyle } from './styled';
import { Image } from 'components';
import { useEffect } from 'react';

//https://react-dropzone.js.org/#section-basic-example

export default function DropZone({ className, children, isDropAccept, isShowUploadFileList, File, ...props }) {
  const [files, setFiles] = useState([]);
  const { acceptedFiles, getRootProps, getInputProps } = useDropzone({
    //disabled: true
    onDrop: (event, files) => {
      !isDropAccept && event.stopPropagation();
    },
    ...props,
  });

  useEffect(() => {
    File ? setFiles(File) : setFiles(acceptedFiles);
  }, [acceptedFiles, File]);

  function renderFileTypeImage(fileType) {
    switch (fileType) {
      case 'csv':
        return 'csv';
      default:
        return 'unknow';
    }
  }

  return (
    <DropZoneStyle className={classNames('dropzone-outer-wrapper col-md-12', className)}>
      <div className="dropzone-wrapper">
        <div {...getRootProps({ className: 'dropzone' })}>
          <input {...getInputProps()} />
          <p>{children ? children : 'Drag&drop some files here, or click to select files'}</p>
        </div>
      </div>

      {isShowUploadFileList && (
        <div className="dropzone-display-wrapper">
          {files.map((file) => (
            <div className="file-list-wrapper" key={file.path}>
              <Image src={`images/icons/icon-file-type-${renderFileTypeImage(file.name.split('.').pop())}.svg`} className="file-type-icon" />

              <div className="file-detail">
                <div className="file-name">{file.name}</div>

                <div className="file-size">{file.size} KB</div>
              </div>

              <Image src="images/icons/icon-delete.svg" className="file-delete-icon" onClcik={() => {}} />
            </div>
          ))}
        </div>
      )}
    </DropZoneStyle>
  );
}

DropZone.defaultProps = {
  isShowUploadFileList: false,
};
