import React from 'react';

export default function SVGLOGO({ className, width, height }) {
  return (
    <svg className={className} xmlns="http://www.w3.org/2000/svg" width={width ? width : '217'} height={height ? height : '67'} viewBox="0 0 267 117">
      <g id="Group_2" data-name="Group 2" transform="translate(-26 2)">
        <text id="Sanampra_" data-name="Sanampra     " transform="translate(26 61)" fill="#f2f2f2" fontSize="52" fontFamily="supermarket">
          <tspan x="0" y="0">
            Do
          </tspan>
          <tspan y="0" xmlSpace="preserve" textDecoration="underline">
            phra{'   '}
          </tspan>
        </text>
        <text id="สนามพระ" transform="translate(136 97)" fill="#f3ac28" fontSize="37" fontFamily="supermarket">
          <tspan x="0" y="0">
            ดูพระ
          </tspan>
        </text>
      </g>
    </svg>
  );
}
