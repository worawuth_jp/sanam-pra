import _ from 'lodash';
import React, { useEffect, useRef, useState } from 'react';
import { Carousel } from 'react-bootstrap';
import variables from 'theme/variables';
import { CarouselStyle } from './styled';

export default function MyCarousel({ className, navHeight, headerHeight, footerHeight, items }) {
  const carouselRef = useRef(null);
  const [bodyHeight, setBodyHeight] = useState(0);

  useEffect(() => {
    carouselRef ? setBodyHeight(_.get(carouselRef, 'current.clientHeight', 0)) : null;
  }, [carouselRef, bodyHeight]);
  return (
    <CarouselStyle
      ref={carouselRef}
      itemHeight={_.get(carouselRef, 'current.clientHeight', variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT)}
      headerHeight={headerHeight}
      navHeight={navHeight}
      footerHeight={footerHeight}
      className={className}
    >
      <Carousel>
        {_.map(items, (item, index) => {
          return (
            <Carousel.Item key={`carousel_img_${index}`}>
              <img className="d-block img-banner" src={item.src} alt="First slide" />
              {/* <Carousel.Caption>
                <h3>{item.name}</h3>
                <p>{item.description}</p>
              </Carousel.Caption> */}
            </Carousel.Item>
          );
        })}
      </Carousel>
    </CarouselStyle>
  );
}
