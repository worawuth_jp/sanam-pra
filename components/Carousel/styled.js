import styled, { css } from 'styled-components';
import variables from 'theme/variables';

export const CarouselStyle = styled.div`
  position: absolute;
  max-height: ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT};
  width: 80%;
  left: 5%;
  right: 5%;
  ${(props) => {
    return css`
      top: calc((${`${props.navHeight}px`} + ((${`${40}px`}) / 2)));
    `;
  }}
  border:none;

  .img-banner {
    margin-left: auto;
    margin-right: auto;
    ${(props) => {
      return css`
        height: ${`${props.itemHeight}px`};
      `;
    }};
  }

  @media only screen and (max-width: 768px) {
    max-height: ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT_XS};
  }

  @media only screen and (min-width: 768.1px) and (max-width: 1200px) {
    max-height: ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT_SM};
  }
`;
