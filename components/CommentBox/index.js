import { Button } from 'components';
import React from 'react';
import { Form, Image, Row } from 'react-bootstrap';
import { CommentBoxStyle, CommentItemStyle } from './styled';

export default function CommentBox({ children, img }) {
  return (
    <CommentBoxStyle>
      <div className="comment-header">
        <h2>แสดงความคิดเห็น</h2>
      </div>
      <div className="comment-body">
        <div>
          <div className="content col-md-12">
            <Row>
              <Image className="mt-2" src={img ? img.src : ''} rounded fluid />
              <Form.Group className="col-md-9" controlId="exampleForm.ControlTextarea1">
                <Form.Control className="mt-2" as="textarea" />
              </Form.Group>
              <Button className="pl-2 pr-2 mt-auto mb-auto my-button-primary">แสดงความคิดเห็น</Button>
            </Row>
          </div>
        </div>
        <div>{children}</div>
      </div>
    </CommentBoxStyle>
  );
}

export function CommentItem({ className, img, children, name }) {
  return (
    <CommentItemStyle className={className}>
      <div className="content">
        <Row>
          <Image src={img ? img.src : ''} rounded fluid />
          <Form.Group className="col-md-10" controlId="exampleForm.ControlTextarea1">
            <div className="content-item">
              <Form.Label className="p-0 mb-0">{name}</Form.Label>
              <span className="mt-0">{children}</span>
            </div>
          </Form.Group>
        </Row>
      </div>
    </CommentItemStyle>
  );
}
