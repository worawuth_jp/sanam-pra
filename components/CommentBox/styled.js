import styled from 'styled-components';
import variables from 'theme/variables';

export const CommentBoxStyle = styled.div`
  width: 100%;
  min-height: 400px;
  max-height: 600px;
  overflow-y: auto;
  background: ${variables.COLOR.WHITECOLOR};
  border: solid thin ${variables.COLOR.GRAY_5};

  button {
    max-height: 36px;
  }

  .comment-header {
    text-align: center;
    width: 100%;
    border-bottom: solid thin ${variables.COLOR.GRAY_5};
  }

  .comment-body {
    margin-left: 15px;
    width: 100%;
    padding: 10px;

    img {
      width: 50px;
      height: 50px;
    }

    .content {
      margin-left: 15px;
      display: block;
      min-height: 50px;
      width: 80%;
    }
  }
`;

export const CommentItemStyle = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 100%;
  padding: 10px;
  img {
    width: 50px;
    height: 50px;
  }

  .content {
    margin-left: 15px;
    display: block;
    min-height: 50px;
  }

  .content-item {
    display: inline-block;
    background: ${variables.COLOR.YELLOW_1};
    padding: 2px 10px 2px 10px;
    font-size: 16px;
    color: black;
    font-weight: bold;

    span {
      font-weight: normal;
      font-size: 17px;
      display: block;
      padding-top: 0px;
    }
  }
`;
