import styled from 'styled-components';
import VAR from 'theme/variables';

export const NavbarStyle = styled.div`
  position: fixed;
  z-index: 1000;
  width: 100%;
  background-color: ${VAR.COLOR.NavThemeColor};

  .linkText {
    color: white;
  }

  .nav-dropdown {
    color: black;
    background-color: white;
  }

  .nav-blue {
    background-color: ${VAR.COLOR.PRIMARY};
    position: fixed;
    width: 100%;
    height: ${VAR.ELEMENTSIZE.NAVBAR_HEIGHT};
  }
`;
