import classNames from 'classnames';
import SVGLOGO from 'components/SVGLOGO';
import React, { useEffect, useRef } from 'react';
import { Nav, Navbar } from 'react-bootstrap';
import { NavbarStyle } from './styled';

export default function MyNavbar({ className, setRef, expand }) {
  const navRef = useRef(null);
  useEffect(() => {
    setRef ? setRef(navRef) : null;
  }, [navRef]);
  return (
    <NavbarStyle ref={navRef}>
      {/* <div className="nav-blue"></div> */}
      <Navbar className={(classNames(className), 'p-0')} variant="dark" expand={expand}>
        <Navbar.Brand className="ml-md-3 ml-xs-1" href="#home">
          <SVGLOGO />
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" className="mr-3" />
        <Navbar.Collapse id="basic-navbar-nav" className="pr-3">
          <Nav.Link className="linkText ml-md-auto " href="#home">
            หน้าแรก
          </Nav.Link>

          <Nav.Link className="linkText" href="#link">
            สนามพระ
          </Nav.Link>

          <Nav.Link className="linkText" href="#home">
            กฎระเบียบ
          </Nav.Link>

          <Nav.Link className="linkText" href="#link">
            ติดต่อเรา
          </Nav.Link>

          <Nav.Link className="linkText" href="#link">
            เข้าสู่ระบบ
          </Nav.Link>
        </Navbar.Collapse>
      </Navbar>
    </NavbarStyle>
  );
}
