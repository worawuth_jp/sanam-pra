import styled from 'styled-components';
import variables from 'theme/variables';

export const LayoutStyle = styled.div`
  background-color: ${variables.COLOR.GRAY_3} !important;
  .search-nav {
    display: flex;
    background-color: ${variables.COLOR.HeaderNavSearch};
    height: ${variables.ELEMENTSIZE.SEARCH_HEIGHT};
  }

  .search-nav .my-button-secorndary:hover {
    background: ${variables.COLOR.NavThemeColor};
  }

  .dropdown:focus {
    outline: none !important;
  }

  .search-btn {
    height: 36px !important;
    background: ${variables.COLOR.PRIMARY_TEXT_COLOR};
    border-top-right-radius: 10px !important;
    border-bottom-right-radius: 10px !important;
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
    border: none;

    &:hover {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
      height: 36px !important;
      background: ${variables.COLOR.NavThemeColor};
    }

    &:focus {
      outline: none !important;

      &:after {
        outline: none !important;
      }
    }
  }

  .filter {
    height: 36px !important;
    background: ${variables.COLOR.PRIMARY_TEXT_COLOR};
    border-top-left-radius: 10px !important;
    border-bottom-left-radius: 10px !important;
    border: none;

    &:focus {
      outline: none !important;

      &:after {
        outline: none !important;
      }
    }

    &::active {
      outline: none !important;
    }
    &:hover {
      border-top-left-radius: 10px;
      border-bottom-left-radius: 10px;
      height: 36px !important;
      background: ${variables.COLOR.NavThemeColor};
    }
  }

  .my-input {
    border-radius: 10px;
    height: 36px;
  }

  .icon {
    color: ${variables.COLOR.PRIMARY_TEXT_COLOR};
    margin-top: auto;
    margin-bottom: auto;
    font-size: ${variables.TYPOGRAPHYS.FONT_SIZE_PX.LBG};
  }

  .icon:last-child {
    margin-right: auto;
  }

  .main {
    display: block;
    background: ${variables.COLOR.GRAY_3};
  }

  .main-content-wrapper {
    min-height: ${variables.ELEMENTSIZE.CONTENT_MIN_HEIGHT};
    width: 80%;
    padding-top: ${variables.ELEMENTSIZE.PADDING_MD};
    padding-bottom: ${variables.ELEMENTSIZE.PADDING_MD};
    margin-left: auto;
    margin-right: auto;
  }

  .white-content {
    margin-top: ${variables.ELEMENTSIZE.MARGIN_MD};
    margin-bottom: ${variables.ELEMENTSIZE.MARGIN_MD};
    background: ${variables.COLOR.WHITECOLOR};
  }

  .search-nav button,
  input,
  div {
    margin-top: auto;
    margin-bottom: auto;
  }

  @media only screen and (max-width: 768px) {
    .search-nav {
      height: ${variables.ELEMENTSIZE.SEARCH_HEIGHT_XS};
    }

    .white-content {
      margin-top: ${variables.ELEMENTSIZE.MARGIN_XS};
      margin-bottom: ${variables.ELEMENTSIZE.MARGIN_XS};
      padding-top: ${variables.ELEMENTSIZE.PADDING_XS};
      padding-bottom: ${variables.ELEMENTSIZE.PADDING_XS};
      background: ${variables.COLOR.WHITECOLOR};
    }
    .main-content-wrapper {
      min-height: ${variables.ELEMENTSIZE.CONTENT_MIN_HEIGHT_XS};
    }
  }

  @media only screen and (max-width: 1200px) and (min-width: 769px) {
    .search-nav {
      height: ${variables.ELEMENTSIZE.SEARCH_HEIGHT_SM};
    }

    .white-content {
      margin-top: ${variables.ELEMENTSIZE.MARGIN_SM};
      margin-bottom: ${variables.ELEMENTSIZE.MARGIN_SM};
      padding-top: ${variables.ELEMENTSIZE.PADDING_SM};
      padding-bottom: ${variables.ELEMENTSIZE.PADDING_SM};
      background: ${variables.COLOR.WHITECOLOR};
    }

    .main-content-wrapper {
      min-height: ${variables.ELEMENTSIZE.CONTENT_MIN_HEIGHT_SM};
    }
  }
`;
