import classNames from 'classnames';
import { Button, Header, Footer } from 'components';
import _ from 'lodash';
import React, { useEffect, useRef } from 'react';
import { LayoutStyle } from './styled';
import EmailIcon from '@material-ui/icons/Email';
import NotificationsIcon from '@material-ui/icons/Notifications';
import SearchIcon from '@material-ui/icons/Search';
import { Dropdown, DropdownButton, FormControl, InputGroup } from 'react-bootstrap';

export default function Layout({ className, children, WhiteContent }) {
  const mainRef = useRef(null);

  useEffect(() => {
    console.log(mainRef);
  }, [mainRef]);
  return (
    <LayoutStyle className={classNames('layout', className)}>
      <Header />
      <div className="search-nav">
        <Button className="my-button-secorndary ml-auto">รายการ</Button>
        <InputGroup className="col-md-5 ml-5 my-input">
          <DropdownButton className="filter" as={InputGroup.Prepend} variant="custom" title="พระเครื่อง" id="input-group-dropdown">
            <Dropdown.Item href="#">Action</Dropdown.Item>
            <Dropdown.Item href="#">Another action</Dropdown.Item>
            <Dropdown.Item href="#">Something else here</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item href="#">Separated link</Dropdown.Item>
          </DropdownButton>
          <FormControl className="my-input col-md-12" aria-describedby="basic-addon1" />
          <InputGroup.Append>
            <Button className="search-btn text-left" variant="custom">
              <SearchIcon />
              ค้นหา
            </Button>
          </InputGroup.Append>
        </InputGroup>
        <EmailIcon className="icon ml-5" />
        <NotificationsIcon className="ml-5 icon" />
      </div>

      <div ref={mainRef} className={classNames('main')}>
        <div className={classNames('main-content-wrapper', { 'white-content': WhiteContent })}>{children}</div>
        <Footer mainTop={_.get(mainRef, 'current.offsetTop')} mainHeight={_.get(mainRef, 'current.offsetHeight')} />
      </div>
    </LayoutStyle>
  );
}
