import SVGLOGO from 'components/SVGLOGO';
import React from 'react';
import { Col, Row } from 'react-bootstrap';
import { FooterStyle } from './styled';

export default function Footer({ className, mainHeight, mainTop }) {
  return (
    <FooterStyle mainTop={mainTop} mainHeight={mainHeight} className={className}>
      <Row className="col-md-12 p-0 mt-0">
        <Col md="4">
          <SVGLOGO width="394" height="120" />
          <div className="footer-content pl-5 pr-5">
            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          </div>
        </Col>
        <Col md="4">
          <div className="underline pl-5 pr-5 header">
            <h3>หน้า</h3>
          </div>

          <div className="footer-content pl-5 pr-5">-> หน้าแรก</div>
        </Col>
        <Col md="4">
          <div className="pl-5 pr-5 header">
            <h3>ติดต่อเรา</h3>
          </div>
          <div className="footer-content pl-5 pr-5">
            xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx.xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
          </div>
        </Col>
      </Row>
    </FooterStyle>
  );
}
