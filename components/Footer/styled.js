import styled, { css } from 'styled-components';
import variables from 'theme/variables';

export const FooterStyle = styled.div`
  position: relative;
  /* ${(props) => {
    console.log(props.mainHeight);
    console.log(props.mainTop);
    return css`
      top: calc((${`${props.mainTop}px`} + ${`${props.mainHeight}px`}));
    `;
  }} */
  height: ${variables.ELEMENTSIZE.FOOTER_HEIGHT};
  width: 100%;
  background: ${variables.COLOR.FooterThemeColor};

  div {
    color: ${variables.COLOR.PRIMARY_TEXT_COLOR};
  }

  div .footer-content {
    white-space: pre-line;
    word-break: break-all;
    min-height: 200px;
  }

  .underline h3 {
    display: block;
    border-bottom-style: solid;
  }

  .header {
    position: relative;
    padding-top: 0px;
    margin-top: 0px;
  }
`;
