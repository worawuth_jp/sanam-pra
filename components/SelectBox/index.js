import React from 'react';
import Select from 'react-select';

export default function SelectBox({ ...props }) {
  const styles = {
    control: (base) => ({
      ...base,
      fontFamily: 'supermarket',
      fontSize: '17px',
    }),
    menu: (base) => ({
      ...base,
      fontFamily: 'supermarket',
      fontSize: '17px',
    }),
  };
  return (
    <Select
      styles={styles}
      theme={(theme) => ({
        ...theme,
        borderRadius: 10,
        fontSize: 17,
        control: {
          height: 36,
          fontFamily: 'Times New Roman',
          fontSize: '17px',
        },
      })}
      {...props}
    />
  );
}
