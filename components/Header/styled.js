import styled, { css } from 'styled-components';
import variables from 'theme/variables';

export const HeaderStyle = styled.div`
  width: 100%;
  ${(props) => {
    return css`
      height: calc((${`${props.navHeight + props.footerHeight + 40}px`} + ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT}));
    `;
  }}
  //height: ${variables.ELEMENTSIZE.BANNER_HEIGHT};
  background-color: ${variables.COLOR.HeaderThemeColor};

  @media only screen and (max-width: 768px) {
    ${(props) => {
      return css`
        height: calc((${`${props.navHeight + props.footerHeight + 40}px`} + ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT_XS}));
      `;
    }}
  }

  @media only screen and (min-width: 768.1px) and (max-width: 1200px) {
    ${(props) => {
      return css`
        height: calc((${`${props.navHeight + props.footerHeight + 40}px`} + ${variables.ELEMENTSIZE.BANNER_ITEM_HEIGHT_SM}));
      `;
    }}
  }
`;
