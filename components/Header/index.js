import MyCarousel from 'components/Carousel';
import CategoryNav from 'components/CategoryNav';
import MyNavbar from 'components/Navbar';
import _ from 'lodash';
import React, { useRef, useState } from 'react';
import variables from 'theme/variables';
import { HeaderStyle } from './styled';

export default function Header() {
  const [navRef, setNavRef] = useState(null);
  const [footer, setFooter] = useState(null);
  const banners = [
    {
      src: 'https://images.ctfassets.net/hrltx12pl8hq/7JnR6tVVwDyUM8Cbci3GtJ/bf74366cff2ba271471725d0b0ef418c/shutterstock_376532611-og.jpg',
      name: 'XXXXXXXXXXXX',
      description: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    },
    {
      src: 'https://images.ctfassets.net/hrltx12pl8hq/7JnR6tVVwDyUM8Cbci3GtJ/bf74366cff2ba271471725d0b0ef418c/shutterstock_376532611-og.jpg',
      name: 'XXXXXXXXXXXX',
      description: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    },
  ];
  const headerRef = useRef(null);
  return (
    <HeaderStyle
      footerHeight={_.get(footer, 'current.clientHeight', 100)}
      navHeight={navRef ? navRef.current.clientHeight : 0}
      ref={headerRef}
      className="header"
    >
      <MyNavbar setRef={setNavRef} className="nav-select" bg="white" expand="md" />
      <MyCarousel
        headerHeight={_.get(headerRef, 'current.clientHeight', variables.ELEMENTSIZE.BANNER_HEIGHT)}
        footerHeight={_.get(footer, 'current.clientHeight', 100)}
        navHeight={navRef ? navRef.current.clientHeight : 0}
        className="ml-auto mr-auto col-md-12"
        items={banners}
      />
      <CategoryNav setRef={setFooter} />
    </HeaderStyle>
  );
}
