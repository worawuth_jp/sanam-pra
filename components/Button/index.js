import classNames from 'classnames';
import React from 'react';
import { ButtonStyle } from './styled';

export default function MYButton({ children, className, isCenter, ...props }) {
  return (
    <ButtonStyle className={classNames(className, { 'ml-auto': isCenter, 'mr-auto': isCenter, 'display-block': isCenter })} {...props}>
      {children}
    </ButtonStyle>
  );
}
