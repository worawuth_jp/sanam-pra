import styled, { css } from 'styled-components';
import variables from 'theme/variables';

export const ButtonStyle = styled.button`
  border-radius: 10px;
  padding: 5px 50px 5px 50px;
  text-align: center;
  font-size: 18px;
  border-color: '#F1F1F1' !important;
  border-width: thin;

  &.none-border {
    border: none !important;
  }

  &.display-block {
    display: block;
  }

  &.bargain-btn {
    background: ${variables.COLOR.GREEN_1};
    //box-shadow: 0 0px #999;
  }
  &.price-btn {
    background: ${variables.COLOR.GREEN_4};
  }

  &.bargain-btn:hover,
  &.price-btn:hover {
    background-color: ${variables.COLOR.GREEN_3};
  }

  &.bargain-btn:active,
  &.price-btn:active {
    background-color: ${variables.COLOR.GREEN_2};
    box-shadow: 0 0px #666;
    transform: translateY(4px);
  }
`;
