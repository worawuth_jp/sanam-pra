import classNames from 'classnames';
import { Button } from 'components';
import dayjs from 'dayjs';
import _ from 'lodash';
import React, { useEffect, useState } from 'react';
import { Col, Image, Row } from 'react-bootstrap';
import { ProductItemStyle, ProductStyle } from './styled';

export default function Product({ products, className, itemPerRow, onClick }) {
  const [cards, setCard] = useState([]);
  const setData = () => {
    let setProduct = [];
    for (let i = 0; i < products.length; ) {
      let temp = [];
      for (let j = 0; j < itemPerRow && i < products.length; j++) {
        temp = [...temp, products[i++]];
      }
      setProduct = [...setProduct, temp];
    }
    setCard([...setProduct]);
  };

  useEffect(() => {
    setData();
  }, []);
  return (
    <ProductStyle className={classNames(className)}>
      {_.map(cards, (card, cardIndex) => {
        return (
          <Row key={`card_${cardIndex}`} className={classNames('product', 'mt-3')}>
            {_.map(card, (product, productIndex) => {
              return <ProductItem onClick={onClick} key={`productItem_${productIndex}`} className={classNames('ml-4')} product={product} />;
            })}
          </Row>
        );
      })}
    </ProductStyle>
  );
}

export function ProductItem({ product, className, onClick }) {
  const date1 = dayjs();
  const date2 = dayjs(product.upload_time);
  const left = date1.diff(date2, 'minutes');
  const minutes_per_day = 1440;
  return (
    <ProductItemStyle className={classNames(className)}>
      <div className="product-header">
        <Row className="pl-1 pr-1">
          <Col md="8">
            <Image className="icon border" src={product.icon} roundedCircle />
            <span className="name">{product.name}</span>
          </Col>

          <Col md="4">
            <span className="upload-time">{left / minutes_per_day > 0 ? `${parseInt(left / minutes_per_day)} วัน` : `${left} นาที`}ที่แล้ว</span>
          </Col>
        </Row>
      </div>
      <div className="product-body">
        <div>
          <Image src={product.pro_img} className="product-img" fluid />
        </div>

        <div className="product-footer">
          <Row>
            <Col md="6">{product.product_name}</Col>
            <Col md="6" className="text-right price-text">
              ราคาล่าสุด <span className="price">{_.get(product, 'prices[0]', 0.0)}</span> บาท
            </Col>
          </Row>
          <Row>
            <Col md="6" className="category">
              {product.category_name}
            </Col>
            <Col md="6" className="text-right category">
              ผู้เข้าชม <span className="view">{_.get(product, 'views', 0)}</span> คน
            </Col>
          </Row>
          <Button isCenter onClick={() => onClick} className="my-button-primary none-border col-md-10 col-sm-10 col-xs-12 mt-2 mb-2">
            ดูรายละเอียด
          </Button>
        </div>
      </div>
    </ProductItemStyle>
  );
}
