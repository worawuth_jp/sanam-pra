import styled from 'styled-components';
import variables from 'theme/variables';

export const ProductStyle = styled.div`
  display: inline;
  border-width: thin;
  width: auto;
  //text-align: center;

  .product {
    display: inline-flex;
    //width: 1010px;
    width: auto;
  }
`;

export const ProductItemStyle = styled.div`
  display: block;
  min-height: 300px;
  min-width: 250px;
  max-height: 400px;
  max-width: 250px;
  background: ${variables.COLOR.WHITECOLOR};
  color: ${variables.COLOR.BLACKCOLOR};

  border: solid ${variables.COLOR.GRAY_7};
  border-width: 0.75px;
  border-radius: 10px;

  .product-header {
    padding: 5px 5px 5px 5px;
    border-bottom: solid 1px ${variables.COLOR.GRAY_7};
  }

  .product-footer {
    padding: 5px 5px 5px 5px;
    border-top: solid 1px ${variables.COLOR.GRAY_7};
  }

  .price-text {
    font-size: 16px;
  }

  .price {
    color: ${variables.COLOR.SECORNDARY_TEXT_COLOR};
    font-weight: bold;
  }

  .category {
    font-size: 15px;
    color: ${variables.COLOR.GRAY_5};
  }

  .upload-time {
    display: flex;
    font-weight: bold;
    font-size: 15px;
  }

  .profile-icon {
    border-radius: 50%;
  }

  .name {
    margin-left: 10px;
  }

  .icon {
    width: 50px;
    height: 50px;
  }

  .product-img {
    width: 400px;
    height: 200px;
  }
`;
